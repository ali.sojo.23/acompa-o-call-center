import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Globalization } from "@ionic-native/globalization/ngx";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TranslateService } from "@ngx-translate/core";
import { Socket } from "ngx-socket-io";

@Component({
	selector: "app-root",
	templateUrl: "app.component.html",
})
export class AppComponent {
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		public router: Router,
		private socket: Socket,
		private globalization: Globalization,
		public traductor: TranslateService
	) {
		this.initializeApp();
	}
	usuario: any;
	token() {
		let token = localStorage.getItem("tk_init");
		let date: number = Date.now();
		let expire: number = +localStorage.getItem("exp_tk");
		this.usuario = localStorage.getItem("import_data");
		console.log(new Date(expire));
		if (token == null) {
			this.router.navigate(["/login"]);
		}
		if (date >= expire) {
			this.router.navigate(["/login"]);
		}
	}

	getdata() {
		let coords: any = { lat: "", lng: "" };
		let lang: string = window.navigator.language.substr(0, 2);

		window.navigator.geolocation.getCurrentPosition(
			(resultado) => {
				coords = {
					lat: resultado.coords.latitude,
					lng: resultado.coords.longitude,
				};

				localStorage.setItem("coordinate", JSON.stringify(coords));
			},
			(error) => {
				console.log(error);
			}
		);

		localStorage.setItem("prefer_lang", lang);
	}

	setDeafultLang() {}

	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
		this.token();
		this.socket.connect();
		this.traductor.setDefaultLang("en");
		this.getdata();
	}
}
