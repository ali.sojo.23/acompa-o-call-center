import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./Guard/auth.guard";
import { LogedGuard } from "./Guard/loged.guard";

const routes: Routes = [
	{
		path: "login",
		loadChildren: "./pages/login/login.module#LoginPageModule",
	},
	{
		path: "forgot-password",
		loadChildren:
			"./pages/forgot-password/forgot-password.module#ForgotPasswordPageModule",
	},
	{ path: "", redirectTo: "dashboard", pathMatch: "full" },
	{
		path: "dashboard",
		loadChildren: "./pages/home/home.module#HomePageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "notifications",
		loadChildren:
			"./pages/notifications/notifications.module#NotificationsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "perfil",
		children: [
			{
				path: ":id",
				loadChildren: "./pages/perfil/perfil.module#PerfilPageModule",
				canActivate: [AuthGuard],
			},
			{
				path: "editar/:id",
				loadChildren:
					"./pages/editar-mi-perfil/editar-mi-perfil.module#EditarMiPerfilPageModule",
				canActivate: [AuthGuard],
			},
		],
	},
	{
		path: "settings",
		canActivate: [AuthGuard],
		children: [
			{
				path: "users",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/user/show/show.module#ShowPageModule",
					},
					{
						path: "create",
						loadChildren:
							"./pages/settings/user/create/create.module#CreatePageModule",
					},
				],
			},
			{
				path: "directions",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/directions/directions.module#DirectionsPageModule",
					},
				],
			},
			{
				path: "rates",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/rates/rates.module#RatesPageModule",
					},
				],
			},
			{
				path: "memberships",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/settings/memberships/show/show.module#ShowPageModule",
					},
					{
						path: "create",
						loadChildren:
							"./pages/settings/memberships/create/create.module#CreatePageModule",
					},
					{
						path: "edit/:id",
						loadChildren:
							"./pages/settings/memberships/edit/edit.module#EditPageModule",
					},
				],
			},
		],
	},
	{
		path: "doctors",
		loadChildren: "./pages/doctors/doctors.module#DoctorsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "notify-chats",
		loadChildren:
			"./pages/notify-chats/notify-chats.module#NotifyChatsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "estudios",
		loadChildren:
			"./pages/perfil/asset/estudios/estudios.module#EstudiosPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "trabajos",
		loadChildren:
			"./pages/perfil/asset/trabajos/trabajos.module#TrabajosPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "maps",
		loadChildren: "./pages/maps/maps.module#MapsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "mobile-menu",
		loadChildren:
			"./pages/mobile-menu/mobile-menu.module#MobileMenuPageModule",
	},
	{
		path: "chat",
		loadChildren: "./pages/chat/chat.module#ChatPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "memberships",
		canActivate: [AuthGuard],
		children: [
			{
				path: "",
				redirectTo: "contracts",
				pathMatch: "full",
			},
			{
				path: "contracts",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/membresias/users/users.module#UsersPageModule",
					},
					{
						path: ":user",
						children: [
							{
								path: "",
								loadChildren:
									"./pages/membresias/contracts/contracts.module#ContractsPageModule",
							},
							{
								path: ":country",
								children: [
									{
										path: "",
										loadChildren:
											"./pages/membresias/membresias/membresias.module#MembresiasPageModule",
									},
								],
							},
						],
					},
				],
			},
		],
	},
	{
		path: "accounting",
		canActivate: [AuthGuard],
		children: [
			{
				path: "orders",
				children: [
					{
						path: ":id",
						loadChildren:
							"./pages/accounting/orders/show/show.module#ShowPageModule",
					},
				],
			},
		],
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
