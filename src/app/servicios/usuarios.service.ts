import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

@Injectable({
	providedIn: "root",
})
export class UsuariosService {
	constructor(private httpClient: HttpClient) {}

	url = environment.urlHost;
	urlUsuarios: string = this.url + "user/";
	header: any;
	setHeaders() {
		let token = localStorage.getItem("tk_init");
		this.header = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}

	obtenerUsuarios(): Observable<any> {
		this.setHeaders();
		return this.httpClient.get(this.urlUsuarios, {
			headers: this.header,
		});
	}
	crearUsuario(info): Observable<any> {
		this.setHeaders();
		let post = JSON.stringify(info);

		return this.httpClient.post(this.urlUsuarios, info, {
			headers: this.header,
		});
	}
	mostrarUsuario(identificador): Observable<any> {
		this.setHeaders();
		return this.httpClient.get(this.urlUsuarios + identificador, {
			headers: this.header,
		});
	}
	actualizarUsuarios(data, identificador): Observable<any> {
		this.setHeaders();

		return this.httpClient.post(
			this.urlUsuarios + "edit/" + identificador,
			data,
			{
				headers: this.header,
			}
		);
	}
	modificarAvatar(avatar, identificador): Observable<any> {
		this.setHeaders();
		return this.httpClient.post(
			this.urlUsuarios + "avatar/" + identificador,
			{ avatar: avatar },
			{
				headers: this.header,
			}
		);
	}

	userQuery(data): Observable<any> {
		this.setHeaders();
		return this.httpClient.put(this.urlUsuarios, data, {
			headers: this.header,
		});
	}
}
