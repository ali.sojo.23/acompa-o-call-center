import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	constructor(private httpClient: HttpClient) {}

	isLogged: boolean = false;
	url = environment.urlHost;
	urlRegistro: string = this.url + "auth/register";
	urlLogin: string = this.url + "auth/login";
	urlResetPassword: string = this.url + "auth/forgot/email";
	urlVerify: string = this.url + "auth/verify/resend/";

	iniciarSesion(data: any): Observable<any> {
		let Json = JSON.stringify(data);

		let headers = new HttpHeaders().set("Content-Type", "application/json");

		return this.httpClient.post(this.urlLogin, Json, { headers: headers });
	}
	registroUsuario(data: any): Observable<any> {
		let Json = JSON.stringify(data);

		let headers = new HttpHeaders().set("Content-Type", "application/json");

		return this.httpClient.post(this.urlRegistro, Json, {
			headers: headers,
		});
	}

	localStorage(data: any) {
		console.log(data);
		let token: string = data.accessToken;
		localStorage.setItem("tk_init", token);
		localStorage.setItem("exp_tk", data.expiresIn);
		localStorage.setItem("inf_tk", data.user.informacion);
		localStorage.setItem("import_data", data.user.Auth);
	}

	verifyToken() {
		let token: string = localStorage.getItem("tk_init");
		if (!token) {
			this.isLogged = false;
		} else {
			this.isLogged = true;
		}
	}

	deletelocalStorage() {
		localStorage.removeItem("tk_init");
		localStorage.removeItem("import_data");
	}

	resetPassword(data: any): Observable<any> {
		let Json = JSON.stringify(data);
		let headers = new HttpHeaders().set("Content-Type", "application/json");
		sessionStorage.setItem("rst_psw_mail", data);
		return this.httpClient.post(this.urlResetPassword, Json, {
			headers: headers,
		});
	}

	verifyEmail(id, token): Observable<any> {
		let headers = new HttpHeaders().set("Content-Type", "application/json");
		return this.httpClient.post(this.urlVerify + id, { headers: headers });
	}
}
