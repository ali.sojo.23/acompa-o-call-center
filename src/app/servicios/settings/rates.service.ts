import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
	providedIn: "root",
})
export class RatesService {
	constructor(private http: HttpClient) {}
	url = environment.urlHost;
	urlRates = this.url + "settings/rates/";
	headers: any;
	setHeaders() {
		let token = localStorage.getItem("tk_init");
		this.headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	get(): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlRates, {
			headers: this.headers,
		});
	}
	getRate(id): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlRates + id, { headers: this.headers });
	}
	postRate(data): Observable<any> {
		console.log(data);

		this.setHeaders();

		return this.http.post(this.urlRates, data, { headers: this.headers });
	}

	putRate(id, data): Observable<any> {
		this.setHeaders();
		return this.http.post(this.urlRates + id, data, {
			headers: this.headers,
		});
	}
}
