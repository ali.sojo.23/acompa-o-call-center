import { Component, OnInit, Input } from "@angular/core";

@Component({
	selector: "app-alerts",
	templateUrl: "./alerts.component.html",
	styleUrls: ["./alerts.component.scss"],
})
export class AlertsComponent implements OnInit {
	@Input() error: any;
	@Input() success: any;
	constructor() {}

	ngOnInit() {}
}
