import { Component, OnInit } from "@angular/core";
import { RatesService } from "src/app/servicios/settings/rates.service";
import * as iso3311a2 from "iso-3166-1-alpha-2";
import { Router } from "@angular/router";

@Component({
	selector: "app-contracts",
	templateUrl: "./contracts.page.html",
	styleUrls: ["./contracts.page.scss"],
})
export class ContractsPage implements OnInit {
	constructor(private ratesService: RatesService, private ruta: Router) {
		this.get();
	}
	country: any;
	get() {
		this.ratesService.get().subscribe(
			(res) => {
				this.country = res;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	setFlag(country) {
		let a = iso3311a2.getCode(country);

		if (!a) {
			a = country.slice(0, 2);
		}

		return "flag-icon-" + a.toLowerCase();
	}
	searchMemberships(id) {
		this.ruta.navigate(["/memberships/contracts", id]);
	}

	ngOnInit() {}
}
