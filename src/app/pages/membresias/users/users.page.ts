import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-users",
	templateUrl: "./users.page.html",
	styleUrls: ["./users.page.scss"],
})
export class UsersPage implements OnInit {
	constructor() {}
	list: boolean = false;
	array: boolean = false;
	user(id) {
		if (id == 1) {
			this.list = true;
			this.array = false;
		}
		if (id == 2) {
			this.list = false;
			this.array = true;
		}
	}

	ngOnInit() {}
}
