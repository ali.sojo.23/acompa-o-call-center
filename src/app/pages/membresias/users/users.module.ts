import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { UsersPage } from "./users.page";
import { ShowComponent } from "./show/show.component";
import { CreateComponent } from "./create/create.component";

const routes: Routes = [
	{
		path: "",
		component: UsersPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	declarations: [UsersPage, ShowComponent, CreateComponent],
})
export class UsersPageModule {}
