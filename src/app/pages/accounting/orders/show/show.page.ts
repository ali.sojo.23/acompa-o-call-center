import { Component, OnInit } from "@angular/core";
import { OrdersService } from "src/app/servicios/accounting/orders.service";
import { ActivatedRoute } from "@angular/router";

@Component({
	selector: "app-show",
	templateUrl: "./show.page.html",
	styleUrls: ["./show.page.scss"],
})
export class ShowPage implements OnInit {
	constructor(private orders: OrdersService, private router: ActivatedRoute) {
		this.getOrder();
	}
	subtotal: number;
	tax: number;
	order: any;
	total: number;
	id = this.router.snapshot.paramMap.get("id");
	getOrder() {
		this.orders.getOne(this.id).subscribe(
			(res) => {
				this.order = res;
				console.log(res);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	subTotal() {
		this.subtotal =
			this.order.family_members *
			this.order.membership.cost *
			this.order.membership.rate.rate;

		return true;
	}
	Tax() {
		this.tax = (this.subtotal * this.order.membership.rate.tax) / 100;
		return true;
	}
	Total() {
		this.total = this.subtotal + this.tax;
		return true;
	}
	cancelar() {
		this.orders.cancelar(this.order._id).subscribe(
			(res) => {
				console.log(res);
				window.history.back();
			},
			(err) => {
				console.log(err);
			}
		);
	}
	ngOnInit() {}
}
