import { Component, OnInit } from "@angular/core";
import { MembershipsService } from "src/app/servicios/settings/memberships.service";
import { Select2OptionData } from "ng2-select2";
import { RatesService } from "src/app/servicios/settings/rates.service";
import { LoadingController } from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
	selector: "app-create",
	templateUrl: "./create.page.html",
	styleUrls: ["./create.page.scss"],
})
export class CreatePage implements OnInit {
	constructor(
		private member: MembershipsService,
		private rates: RatesService,
		public loadingController: LoadingController,
		private router: Router
	) {
		this.getRates();
	}
	memberships: any = {};
	country: Array<Select2OptionData> = [];
	setItems: number = 1;
	error;
	success;
	errorHours: string;
	comprobateHours(i) {
		if (i > 24) {
			this.errorHours =
				"El Grupo de hora seleccionado debe ser menor o igual a 24";
		} else if (i % 8 != 0) {
			this.errorHours =
				"El Grupo de hora seleccionado debe ser multiplo de 8";
		} else {
			this.errorHours = "";
		}
	}
	setArray(i) {
		return Array(i);
	}
	showPlus(i) {
		let b = this.setItems - 1;
		if (i == b) {
			return true;
		}
	}

	plusItem() {
		this.setItems = this.setItems + 1;
	}

	evaluateSkill(i) {
		if (this.memberships.skill === undefined) {
			this.memberships.skill = [{ title: "", hours: "" }];
		} else if (this.memberships.skill.length <= i) {
			this.memberships.skill.push({ title: "", hours: "" });
		}
		return true;
	}
	getRates() {
		this.rates.get().subscribe(
			(res) => {
				console.log(res);
				for (let i = 0; i < res.length; i++) {
					if (i == 0) {
						this.country = [
							{
								id: "",
								text: "Select one country",
								disabled: true,
							},
							{
								id: res[i]._id,
								text: res[i].country.Country,
							},
						];
					} else {
						this.country.push({
							id: res[i]._id,
							text: res[i].country.Country,
						});
					}
				}
			},
			(err) => {
				console.log(err);
			}
		);
	}
	async useAvatar(event) {
		const loading = await this.loadingController.create({
			cssClass: "my-custom-class",
			message: "Please wait...",
			duration: 2000,
		});

		await loading.present();
		let reader: any = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];

			if (file.size > 1000000) {
				await loading.onDidDismiss();
				this.error =
					"Error al cargar la imaágen por favor seleccione una imagen menor a 1Mb";
				return;
			}
			if (file.type != "image/jpeg" && file.type != "image/png") {
				await loading.onDidDismiss();
				this.error =
					"Verifique su tipo de archivo solo puede cargar archivos en formato PNG o JPEG,JPG";
				return;
			}
			reader.readAsDataURL(file);
			reader.onload = async () => {
				this.memberships.avatar =
					"data:image/png;base64," + reader.result.split(",")[1];
				await loading.onDidDismiss();
			};
		}
	}

	save() {
		console.log(this.memberships);
		this.member.create(this.memberships).subscribe(
			(res) => {
				this.success =
					"La membresía fue creada de manera exitosa, será redireccionado a la página inicial";
				setTimeout(() => {
					window.location.href = "/settings/memberships/";
				}, 3000);
			},
			(err) => {
				this.error = err.error.message;

				console.log(err);
			}
		);
	}
	ngOnInit() {}
}
