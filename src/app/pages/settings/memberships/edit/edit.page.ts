import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MembershipsService } from "src/app/servicios/settings/memberships.service";

@Component({
	selector: "app-edit",
	templateUrl: "./edit.page.html",
	styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
	constructor(
		private router: ActivatedRoute,
		private members: MembershipsService,
		private route: Router
	) {
		this.get();
	}
	memberships: any = {};
	setItems: any;
	id: string;
	success: string;
	get() {
		this.id = this.router.snapshot.paramMap.get("id");
		this.members.getById(this.id).subscribe(
			(res) => {
				this.memberships = res;
				this.setItems = this.memberships.skills.length;
				console.log(res);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	update() {
		this.members.UpdateOne(this.id, this.memberships).subscribe(
			(res) => {
				this.success =
					"La membresía fué actualiza con exito, será redirigido a la página anterior";
				setTimeout(() => {
					this.route.navigate(["/settings/memberships"]);
				}, 5000);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	setArray(i) {
		return Array(i);
	}
	showPlus(i) {
		let b = this.setItems - 1;
		if (i == b) {
			return true;
		}
	}
	evaluateSkill(i) {
		if (this.memberships.skills.length <= i) {
			this.memberships.skills.push({ title: "", hours: "" });
		}
		return true;
	}
	plusItem() {
		this.setItems = this.setItems + 1;
		console.log(this.memberships);
	}
	ngOnInit() {}
}
