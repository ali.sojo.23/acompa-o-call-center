import { Component, OnInit } from "@angular/core";
import { AddressService } from "src/app/servicios/address.service";
import { ModalController } from "@ionic/angular";
import { ModalComponent } from "./modal/modal.component";
import * as iso3311a2 from "iso-3166-1-alpha-2";

@Component({
	selector: "app-directions",
	templateUrl: "./directions.page.html",
	styleUrls: ["./directions.page.scss"],
})
export class DirectionsPage implements OnInit {
	constructor(
		private directions: AddressService,
		private modal: ModalController
	) {
		this.getCountry();
	}
	countries: any;
	country: any;
	cities: any;
	getCountry() {
		this.directions.obtenerPaises().subscribe(
			(res) => {
				this.countries = res.country;
			},
			(err) => {
				console.log(err);
			}
		);
	}
	search() {
		this.directions.obtenerCiudades(this.country).subscribe(
			(res) => {
				this.cities = res.province;
			},
			(err) => {
				console.log(err);
			}
		);
	}

	async showModal(type: string) {
		let country, city;
		if (type == "city") {
			city = true;
		} else {
			country = true;
		}
		let modal = await this.modal.create({
			component: ModalComponent,
			componentProps: {
				modalCountry: country,
				pais: this.country,
				modalCity: city,
			},
		});

		return modal.present();
	}

	ngOnInit() {}
}
