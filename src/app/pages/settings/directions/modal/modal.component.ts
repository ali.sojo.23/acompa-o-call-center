import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { AddressService } from "src/app/servicios/address.service";

@Component({
	selector: "app-modal",
	templateUrl: "./modal.component.html",
	styleUrls: ["./modal.component.scss"],
})
export class ModalComponent implements OnInit {
	constructor(
		private modal: ModalController,
		private address: AddressService
	) {}
	@Input() modalCountry: boolean;
	@Input() modalCity: boolean;
	@Input() pais: any;
	country: any;
	city: string;
	error: string;
	success: string;
	response(type: string, res) {
		this.success = "el " + type + " fue creado con exito";
		setTimeout(() => {
			this.dismiss();
		}, 1000);
	}
	createCountry() {
		let country = {
			country: this.country,
		};
		this.address.crearPais(country).subscribe(
			(res) => {
				this.response("Pais", res);
			},
			(err) => console.log(err)
		);
	}
	createCity() {
		let city = {
			province: this.city,
			country: this.pais,
		};
		this.address.crearCiudades(city).subscribe(
			(res) => {
				this.response("Ciudad", res);
			},
			(err) => {
				console.log(err);
			}
		);
	}
	dismiss() {
		// using the injected ModalController this page
		// can "dismiss" itself and optionally pass back data
		this.modal.dismiss({
			dismissed: true,
		});
	}

	ngOnInit() {}
}
